import Vue from 'vue'
import Router from 'vue-router'
import Vuetify from 'vuetify'
import Hello from '@/components/Hello'
import About from '@/components/About'
import Notes from '@/components/Notes'
import Simpletodo from '@/components/todo'

Vue.use(Vuetify)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/About',
      name: 'About',
      component: About
    },
    {
      path: '/Notes',
      name: 'Notes',
      component: Notes
    },
    {
      path: '/Simpletodo',
      name: 'Simpletodo',
      component: Simpletodo
    }
  ]
})
