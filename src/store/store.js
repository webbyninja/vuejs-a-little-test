import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  newTodo: '',
  todos: [],
  notes: [],
  activeNote: {}
}

const mutations = {
  GET_TODO (state, todo) {
    state.newTodo = todo
  },
  ADD_TODO (state) {
    state.todos.push({
      body: state.newTodo,
      completed: false
    })
  },
  EDIT_TODO (state, todo) {
    var todos = state.todos
    todos.splice(todos.indexOf(todo), 1)
    state.todos = todos
    state.newTodo = todo.body
  },
  REMOVE_TODO (state, todo) {
    var todos = state.todos
    todos.splice(todos.indexOf(todo), 1)
  },
  COMPLETE_TODO (state, todo) {
    todo.completed = !todo.completed
  },
  CLEAR_TODO (state) {
    state.newTodo = ''
  },

  ADD_NOTE (state) {
    const newNote = {
      text: 'New note',
      favorite: false
    }
    state.notes.push(newNote)
    state.activeNote = newNote
  },

  EDIT_NOTE (state, text) {
    state.activeNote.text = text
  },

  DELETE_NOTE (state) {
    // state.notes.$remove(state.notes[0])
    let index = state.notes.indexOf(state.activeNote)
    state.notes.splice(index, 1)
    state.activeNote = state.notes[0]
  },

  TOGGLE_FAVORITE (state) {
    state.activeNote.favorite = !state.activeNote.favorite
  },

  SET_ACTIVE_NOTE (state, note) {
    state.activeNote = note
  }
}

const actions = {
  getTodo ({ commit }, todo) {
    commit('GET_TODO', todo)
  },
  addTodo ({ commit }) {
    commit('ADD_TODO')
  },
  editTodo ({ commit }, todo) {
    commit('EDIT_TODO', todo)
  },
  removeTodo ({ commit }, todo) {
    commit('REMOVE_TODO', todo)
  },
  completeTodo ({ commit }, todo) {
    commit('COMPLETE_TODO', todo)
  },
  clearTodo ({ commit }) {
    commit('CLEAR_TODO')
  },

  addNote ({ commit }) {
    console.log('committed added note!')
    commit('ADD_NOTE')
  },
  editNote ({ commit }, value) {
    commit('EDIT_NOTE', value)
  },
  deleteNote ({ commit }) {
    commit('DELETE_NOTE')
  },
  updateActiveNote ({ commit }, note) {
    commit('SET_ACTIVE_NOTE', note)
  },
  toggleFavorite ({ commit }) {
    commit('TOGGLE_FAVORITE')
  }

}

const getters = {
  newTodo: state => state.newTodo,
  todos: state => state.todos.filter((todo) => { return !todo.completed }),
  completedTodos: state => state.todos.filter((todo) => { return todo.completed }),
  notes: state => state.notes,
  activeNote: state => state.activeNote,
  activeNoteText: state => state.activeNote.text
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
